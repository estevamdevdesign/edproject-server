const multer = require("multer");
const path = require("path");
const  crypto = require("crypto");

var createFile = {
    post: (main_path) =>{
        return {
            dest: path.resolve(__dirname, '..', '..', 'tmp'),
            storage: multer.diskStorage({
                destination: (req, file, callback) => {

                    let main_folder = "main_" + req.params.id
                    let folder = "_" + req.params.folder_id

                    callback(null, path.resolve(__dirname, '..', '..', 'tmp', main_folder, folder));
                },
                filename: (req, file, callback) => {
                    crypto.randomBytes(16, (err, hash) =>{
                        if(err){
                            callback(err);
                        }else{
                            file.key = `${hash.toString('hex')}-${file.originalname}`;
                            callback(null, file.key)
                        }
                    })
                }
            })
        }
    }
}

module.exports = createFile;