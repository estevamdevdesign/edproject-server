const User = require("../models/user");
const MainBox = require("../models/main_box");
const Box = require("../models/box");
const FileController = require("../controllers/file");
const fs = require("fs");
const path = require("path");


class BoxController{
    async store(req, res){
        const user = await User.findById({_id: req.params.id}, function(err, user){
            if(user){
                const box = Box.create({
                    title: req.body.title,
                    user_id: req.params.id
                }, function(err, box){
                    if(box){
                        const mainBox = MainBox.findOne({user_id: req.params.id}, function(err, main_box){
                            if(main_box){
                                main_box.boxes.push(box)
                                MainBox.updateOne({user_id: req.params.id}, {$set : {boxes: main_box.boxes}}, function(err, main){
                                    var dir = path.resolve(__dirname, '..', '..', 'tmp', main_box.title);
                                    if(fs.existsSync(dir)){
                                        var sub_dir = path.resolve(__dirname, '..', '..', 'tmp', main_box.title, "_" + box._id)
                                        if(!fs.existsSync(sub_dir)){
                                            fs.mkdirSync(sub_dir)
                                            res.send(box)
                                        }
                                    }
                                })
                            }
                        })
                    }
                })
            }else{
                res.send("User not exist.")
            }
        })

    }

    async get(req, res){
        const box = await Box.find({user_id: req.params.id});
        return res.send(box);
    }

    async show(req, res){
        const box = await Box.findById(req.params.id).populate({
            path: 'files',
            options: {
                sort: {
                    createdAt: -1
                }
            }
        });
        return res.json(box);
    }

    async clearFolder(req, res){
        const user = await User.findById({_id: req.params.id}, function(err, user){
            const main_box = MainBox.updateOne({user_id: req.params.id}, {$pull:{boxes: req.params.folder_id}}, function(err, main){
                if(main){
                    Box.findOne({_id: req.params.folder_id, user_id: req.params.id}, function(err, current_box){
                        if(current_box){
                            Box.deleteOne({_id: req.params.folder_id, user_id: req.params.id}, function(err, box){
                                if(box){
                                    let main_path = "main_" + user._id
                                    let sub_dir = path.resolve(__dirname, '..', '..', 'tmp', main_path, "_" + current_box._id)
                                    if(fs.existsSync(sub_dir)){
                                        fs.rmdir(path.resolve(__dirname, '..', '..', 'tmp', main_path, "_" + current_box._id), function(e,s){
                                            if(e){
                                                res.send(e)
                                            }else{
                                                Box.find({user_id: req.params.id}, function(err, boxes){
                                                    res.json(boxes)
                                                })
                                                // res.send("Folder been deleted")
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    })
                }
            })
        })
    }

    async clear(req, res){
        
    }
}

module.exports = new BoxController();// return instance class