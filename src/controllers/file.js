const express = require("express");
const path = require("path")
const User = require("../models/user");
const Box = require("../models/box");
const File = require("../models/file");
const fs = require("fs");

// class FileController{

//     async store(req, res){

//     //     console.log(req.file)

        
//     //     await User.findById(req.params.id, function(err, user){
//     //         if(user){
//     //             // const box = Box.findOne({_id: req.params.folder_id, user_id: req.params.id}, function(err, box){
//     //             //     console.log(box)
//     //             //     const file = File.create({
//     //             //         title: req.file.originalname,
//     //             //         path: req.file.key,
//     //             //     })

//     //             //     box.files.push(file)

//     //             //     box.save()

//     //             //     req.io.sockets.in(box._id).emit('file', file);

//     //             // })

//     //             // console.log(req.file)

//     //             // console.log(req)


//     //             const box = Box.findOne({_id: req.params.folder_id}, function(err, box){
//     //                 // console.log(user.main_box[0])
//     //                 // console.log(box)

//     //                 // var main_path = path.resolve(__dirname, "..", "..", "tmp", "main_" + user.main_box[0], "_" + box._id)


//     //                 var main_path = path.resolve(__dirname, "..", "..", "tmp")
//     //                 multer(multerConfig.post(main_path)).single('file')

//     //                 // const file = File.create({
//     //                 //     title: req.file.originalname,
//     //                 //     path: req.file.key,
//     //                 // })   

//     //                 // box.files.push(file)

//     //                 // box.save();

//     //             });


//     //             // const file = File.create({
//     //             //     title: req.file.originalname,
//     //             //     path: req.file.key,
//     //             // })


//     //             // box.files.push(file);
                
//     //             // box.save();

//     //             // req.io.sockets.in(box._id).emit('file', file);

//     //             // return res.json(file)
//     //         }
//     //     })

//     //     // console.log(user)


//     //     // const box = await Box.findById(req.params.id);
//     //     // const file = await File.create({
//     //     //     title: req.file.originalname,
//     //     //     path: req.file.key,
//     //     // })

//     //     // box.files.push(file);
        
//     //     // await box.save();

//     //     // req.io.sockets.in(box._id).emit('file', file);

//     //     // return res.json(file)
//         return "main_5cbb60838071072cfce762a9/_5cbb60bb8071072cfce762b5"

//     }
// }

// module.exports = new FileController();// return instance class

class FileController{
    async store(req, res){
        var box = await Box.findOne({_id: req.params.folder_id, user_id: req.params.id}, function(err, box){
            
            const main_directory = path.join(".","main_" + box.user_id, "_" + box._id);
            
            const file = File.create({
                title: req.file.originalname,
                path: req.file.key,
                directory: main_directory
            }, function(err, file){
                box.files.push(file)
                box.save()
                req.io.sockets.in(box._id).emit('file', file);
                return res.json(file)
            })
        })
    }

    async clear(req, res){
        let user = await User.findById({_id: req.params.id}, function(err, user){
            Box.findOneAndUpdate({_id: req.params.folder_id}, {$pull: {files: req.params.file_id}}, function(err, box){
                if(err){
                    res.send(err)
                }
    
                if(box){
                    File.findOne({_id: req.params.file_id}, function(err, res){
                        if(res){
                            fs.unlink(path.resolve(path.resolve(__dirname, '..', '..', 'tmp', "main_" + user._id, "_" + box._id, res.path)), function(e, s){
                                if(e){
                                    return e
                                }else{
                                    File.deleteOne({_id: req.params.file_id}, function(err, file){
                                        if(err)
                                            res.send(err)
                                    })
                                }
                            })
                        }
                    })
                }
                res.send(box)
            })
        })
    }
}

module.exports = new FileController();