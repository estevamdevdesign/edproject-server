const User = require("../models/user");
const MainBox = require("../models/main_box");
const fs = require("fs");
const path = require("path");

class UserController{
    async store(req, res){
        const user = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        }, function(err, user){
            if(user){
                const box = MainBox.create({
                    title: "main_" + user._id,
                    user_id: user._id
                }, function(err, main_box){
                    if(main_box){
                        user.main_box.push(main_box)
                        user.save();
                        var dir = path.resolve(__dirname, '..', '..', 'tmp', main_box.title);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir)
                        }
                        return res.json(user);
                    }
                })
            }
        })
    }

    async login(req, res){
        await User.findOne({email: req.body.email, password: req.body.password}, function(err, success){
            if(success){
                res.send(success)
            }else{
                res.send("Username or password is incorrect")
            }
        })
    }
    
    async getUser(req, res){
        await User.findOne({_id: req.params.id},function(err, success){
            if(success){

                console.log(success)

                let data = {
                    name:success.name,
                    lastName: success.lastName,
                    genre: success.genre,
                    stateSelected: success.stateSelected,
                    city: success.city,
                    birthDay: success.birthDay,
                    userName: success.userName,
                    email: success.email,
                    adminUser: success.adminUser,
                    neighborhood: success.neighborhood,
                    address: success.address,
                    number: success.number,
                    zipCode: success.zipCode,
                }

                res.json(data)
            }else{
                res.send(err)
            }
        })
    }
}

module.exports = new UserController();// return instance class