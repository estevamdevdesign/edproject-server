const State = require("../models/state");

class StateController{
    async getStates(req, res){
        let states = await State.find()
        res.send(states)
    }
}

module.exports = new StateController();// return instance class