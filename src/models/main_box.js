const mongoose = require("mongoose")

const MainBox = new mongoose.Schema(
    {
        title: {type: String, required: true},
        user_id: {type: String, required: true},
        boxes: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Box"
        }],
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("MainBox", MainBox);