const mongoose = require("mongoose")

const State = new mongoose.Schema(
    {
        value: {
            type: String, 
            
        },
        label: {
            type: String, 
            
        },
    }
);

module.exports = mongoose.model("State", State);