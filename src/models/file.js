const mongoose = require("mongoose")
const urlBase = require("../config/urlbase")

const File = new mongoose.Schema(
    {
        title: {
            type: String, 
            required: true
        },
        path :{
            type: String,
            required: true
        },
        directory:{
            type: String,
            required: false
        }
    },
    {
        timestamps: true,
        toObject: {virtuals: true},
        toJSON: { virtuals: true}
    }
);

File.virtual('url').get(function(){
    return `${urlBase}/files/${this.directory}/${encodeURIComponent(this.path)}`
})

module.exports = mongoose.model("File", File);