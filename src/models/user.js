const mongoose = require("mongoose")

const User = new mongoose.Schema(
    {
        name: {
            type: String, 
            required: true
        },
        email: {
            type: String, 
            required: true
        },
        lastName: {
            type: String, 
            required: false
        },
        genre: {
            type: String, 
            required: false
        },
        stateSelected: {
            type: Object, 
            value: {
                label : {
                    type: String
                },
                value: {
                    type: String
                }
            }
        },
        city: {
            type: String, 
            required: false
        },
        birthDay: {
            type: String, 
            required: false
        },
        userName: {
            type: String, 
            required: true
        },
        adminUser: {
            type: Boolean, 
            required: false
        },

        neighborhood: {
            type: String, 
            required: false
        },
        address: {
            type: String, 
            required: false
        },
        number: {
            type: String, 
            required: false
        },
        zipCode: {
            type: String, 
            required: true
        },
        complement: {
            type: String,
            required: false
        },
        password: {
            type: String, 
            required: true
        },
        main_box: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Box",
        }],
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("User", User);