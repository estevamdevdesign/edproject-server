const express = require('express');
const mongoose = require('mongoose');
const urlBase = require("./config/urlbase")
const path = require("path");
const cors = require("cors");

const app = express();

app.use(cors());

const server = require("http").Server(app)
const io = require("socket.io")(server);

io.on('connection', socket => {
  socket.on('connectRoom', box =>{
    socket.join(box);
  })
})

// mongoose.connect("mongodb://localhost:27017/omnistack", {
//     useNewUrlParser: true
// })

mongoose.connect("mongodb+srv://omnistackmarvel:rafa299102@cluster0-9vg2e.mongodb.net/test?retryWrites=true", {
  useNewUrlParser: true
})

var db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback){
  console.log("Connection Succeeded");
});

app.use((req, res, next)=>{
  req.io = io
  return next();
})


app.use(express.json());
app.use(express.urlencoded({extended: true})); //to submit file in requests
app.use('/files', express.static(path.resolve(__dirname, '..', 'tmp')));
app.use(require('./routes'));

server.listen(process.env.PORT || 4000)