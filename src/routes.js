const express = require("express");
const routes = express.Router();
const multer = require("multer");
const multerConfig = require("./config/multer");

const StateController = require("./controllers/states");
const UserController = require("./controllers/user");
const BoxController = require("./controllers/box");
const FileController = require("./controllers/file");

routes.get("/states", StateController.getStates);

const userService = "/users/:id"

routes.post(`${userService}/folders`, BoxController.store);
routes.get(`${userService}/folders`, BoxController.get);
routes.get(`${userService}/folders/:id`, BoxController.show);
routes.delete(`${userService}/folders/:folder_id`, BoxController.clearFolder)

routes.post(`${userService}/folders/:folder_id/files`, multer(multerConfig.post()).single('file'), FileController.store);
routes.delete(`${userService}/folders/:folder_id/files/:file_id`, FileController.clear)

routes.post("/login",  UserController.login)
routes.post("/users",  UserController.store)
routes.get("/users/:id",  UserController.getUser)

routes.get('/', (req, res) =>{
    return res.send("Hello World")
})

module.exports = routes;